FROM alpine:3.7

COPY kubega /kubega

RUN chown 755 /kubega

EXPOSE 3000

USER nobody

ENTRYPOINT ["/kubega"]
